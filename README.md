# Wayin Helper Chrome Extension

## Intro

I built a tool a while ago that injects a little helper onto the page of our campaigns to find some info. Decided to package it up and see what it took to make a proper Chrome extension out of for the rest of the team to use if they found it useful.

## Installation

Right now this can be added to Chrome via the web store here: https://chrome.google.com/webstore/detail/wayin-experiences-helper/hmilnbfdpondoojallgefiommfiinmha?hl=en. I have to add you to the user list who can download it. Anyone outside of my list will not be allowed to download.

## Information this provides

* Account ID 
* Link to account in M
* Campaign ID
* Link to campaign dashboard: Must be signed into the account. My quick way of doing this is clicking the account ID link, clicking "switch to this account" at the top, and then clicking the campaign dashboard link
* Campaign name
* Theme directory and an attempt to tell you how the campaign was built either themekit, or theme designer

## How these things work

Chrome extensions by default don't have direct access to the DOM and all of its properties by default. You must use the content script (contentscript.js) to inject the actual script you want to run on the page (page.js). That file will then have access to the DOM. I need to reevaluate how necessary popup.js and eventPage.js are. This was my first extension. You'll want to focus on page.js as this is what I am currently injecting into the page.

That file has the html template, CSS, and js required to run the extension. Chrome accepts ES6 syntax and since this is the only browser we're worried about, that's what I'm using. At some point I did have the HTML and CSS unminified, unfortunately I can't find that right now but you should be able to uglify that string if you'd like.

## Stuff I've left out

* I do have a button that I use on my local version that will deploy themes for whatever account you're in at the moment. It can be useful for 95% of accounts if you're working on a themekit theme, you can be testing locally, commit, and just go back to the campaign and click that button and it will deploy. I did not use it for this version because I didn't want non-devs to have that power.

## Documentation

General extensions documentation: https://developer.chrome.com/extensions
More about programmatic injection: https://developer.chrome.com/extensions/content_scripts#pi