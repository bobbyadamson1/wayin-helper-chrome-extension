const injectScript = (file, node, callback) => {
    let el = document.querySelector(node);
    let s = document.createElement('script');

    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', file);
    el.appendChild(s);

    s.onload = callback;
}

const loadImageAssets = () => {
    const logo = new Image();
    logo.src = chrome.extension.getURL('img/logo.jpg');
    document.querySelector('.openMenu').insertBefore(logo, document.querySelector('.openMenu p'));

}

injectScript(chrome.extension.getURL('page.js'), 'body', loadImageAssets);
