const WYNEXPHELPER = (function(){
    // Need a template to inject onto the page
    const template = `<a href="#" class="closeMenu" data-extensionImage="needsClose">X</a> <div class="inner"> <ul id="basicInfo"> <li> Acct #: <span data-info="acctId"></span> <a href="#" data-info="acctLink" target="_blank" data-extensionImage="needsRightArrow" data-bg-image="img/arrow.png" title="Link to account in M">></a> </li><li> Campaign ID: <span data-info="campaignId"></span> <a href="#" target="_blank" data-info="campaignLink" data-extensionImage="needsRightArrow" data-bg-image="img/arrow.png" title="Link to Campaign in console, must be logged into account">></a> </li></ul> <ul id="mainMenu"><!--<li> <a href="#">Deploy Themes</a> <ul> <li>Are you sure? <a href="#">Yes</a> <a href="#">Cancel</a> </li></ul> </li>--> <li> <a href="#" data-extensionImage="needsRightArrow" data-bg-image="img/arrow.png">Campaign Info</a> <ul class="info"> <li>Acct #: <span data-info="acctId"></span></li><li>Campaign #: <span data-info="campaignId"></span></li><li>Campaign Name: <span data-info="campaignName"></span></li><li>Theme Directory: <span data-info="themeDirectory"></span></li></ul> </li></ul> </div><a href="#" class="openMenu"> <p>HLPR</p></a>`;

    // Need to style the element
	const styles = `#WYNEXPHELPERContainer{font-size:16px;font-family:Arial,sans-serif;display:inline-block;width:50px;height:50px;text-align:left;background:#fff;color:#474444!important;border-radius:5px;box-shadow:0 3px 10px rgba(0,0,0,.2);position:fixed;bottom:20px;left:20px;z-index:99999999;-webkit-transition:width .2s ease,height .2s ease;-moz-transition:width .2s ease,height .2s ease;-o-transition:width .2s ease,height .2s ease;transition:width .2s ease,height .2s ease}#WYNEXPHELPERContainer a{color:#19bad5}#WYNEXPHELPERContainer:not(.open) .inner>*{-webkit-transform:translateX(-100%);-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-o-transform:translateX(-100%);transform:translateX(-100%);-webkit-transition:-webkit-transform .2s ease;-moz-transition:-moz-transform .2s ease;-o-transition:-o-transform .2s ease;transition:-webkit-transform .2s ease,-moz-transform .2s ease,-o-transform .2s ease,transform .2s ease}#WYNEXPHELPERContainer .inner{overflow:hidden;font-family:Arial;position:relative;background:#fff;height:100%}#WYNEXPHELPERContainer .inner>*{overflow:hidden}#WYNEXPHELPERContainer .closeMenu{position:absolute;top:0;left:0;color:#000;text-decoration:none;font-size:0;padding:10px;-webkit-transition:-webkit-transform .2s ease,rotate .2s ease;-moz-transition:-moz-transform .2s ease,rotate .2s ease;-o-transition:-o-transform .2s ease,rotate .2s ease;transition:-webkit-transform .2s ease,-moz-transform .2s ease,-o-transform .2s ease,transform .2s ease,rotate .2s ease}#WYNEXPHELPERContainer .closeMenu:before{font-family:socialvetica,arial,sans-serif;font-size:16px;content:'\\78';display:block;background-size:contain}#WYNEXPHELPERContainer.open{max-width:250px;min-width:220px;height:auto}#WYNEXPHELPERContainer.open .openMenu{zoom:1;filter:alpha(opacity=0);-webkit-opacity:0;-moz-opacity:0;opacity:0;display:none}#WYNEXPHELPERContainer.open .closeMenu{left:80%;-webkit-transform:translateX(50px) rotate(180deg);-moz-transform:translateX(50px) rotate(180deg);-ms-transform:translateX(50px) rotate(180deg);-o-transform:translateX(50px) rotate(180deg);transform:translateX(50px) rotate(180deg)}#WYNEXPHELPERContainer .openMenu{text-decoration:none;display:block;width:50px;height:50px;font-family:courier,arial,sans-serif;position:absolute;top:0;left:0;text-align:center;overflow:hidden;padding:7px;box-sizing:border-box}#WYNEXPHELPERContainer .openMenu img{width:30px;display:inline-block}#WYNEXPHELPERContainer #basicInfo,#WYNEXPHELPERContainer #basicInfo li a{display:-webkit-box;display:-moz-box;display:-webkit-flex;display:-ms-flexbox}#WYNEXPHELPERContainer .openMenu p{margin:-7px 0 0;font-size:10px}#WYNEXPHELPERContainer ul{list-style:none;padding-left:0}#WYNEXPHELPERContainer #basicInfo{margin:0;display:flex;-webkit-box-direction:normal;-moz-box-direction:normal;-webkit-box-orient:horizontal;-moz-box-orient:horizontal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;width:100%;border-bottom:1px solid #efefef}#WYNEXPHELPERContainer #basicInfo li{width:50%;padding:10px;border-right:1px solid #efefef;position:relative;text-transform:uppercase;font-weight:700;font-size:10px}#WYNEXPHELPERContainer #basicInfo li a:before,#WYNEXPHELPERContainer #mainMenu>li a:after{content:'\\5d';font-family:socialvetica,arial,sans-serif;vertical-align:middle;background-size:contain;width:8px}#WYNEXPHELPERContainer #basicInfo li:last-child{border-right:none}#WYNEXPHELPERContainer #basicInfo li span{display:block;text-transform:none;font-size:16px;font-weight:400}#WYNEXPHELPERContainer #basicInfo li a{font-size:0;display:flex;-webkit-box-align:center;-moz-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;position:absolute;top:0;right:0;height:100%;box-sizing:border-box;padding:10px;text-decoration:none}#WYNEXPHELPERContainer #basicInfo li a:before{font-size:12px;padding-top:4px;display:block;height:22px}#WYNEXPHELPERContainer #mainMenu{position:relative;margin:0}#WYNEXPHELPERContainer #mainMenu>li a{display:-webkit-box;display:-moz-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-moz-box-pack:justify;-ms-flex-pack:justify;-webkit-justify-content:space-between;justify-content:space-between;padding:10px;text-decoration:none;border-bottom:1px solid #efefef}#WYNEXPHELPERContainer #mainMenu>li a:after{-webkit-transform:scale(.5);-moz-transform:scale(.5);-ms-transform:scale(.5);-o-transform:scale(.5);transform:scale(.5);display:inline-block;float:right;position:relative;height:11px;top:2px;-webkit-transition:-webkit-transform .2s ease;-moz-transition:-moz-transform .2s ease;-o-transition:-o-transform .2s ease;transition:-webkit-transform .2s ease,-moz-transform .2s ease,-o-transform .2s ease,transform .2s ease}#WYNEXPHELPERContainer #mainMenu>li a.active:after{-webkit-transform:scale(.5) rotate(90deg);-moz-transform:scale(.5) rotate(90deg);-ms-transform:scale(.5) rotate(90deg);-o-transform:scale(.5) rotate(90deg);transform:scale(.5) rotate(90deg)}#WYNEXPHELPERContainer #mainMenu>li ul{-webkit-transform:translateX(100%);-moz-transform:translateX(100%);-ms-transform:translateX(100%);-o-transform:translateX(100%);transform:translateX(100%);top:0;width:100%;display:block;height:0;-webkit-transition:height .2s ease;-moz-transition:height .2s ease;-o-transition:height .2s ease;transition:height .2s ease}#WYNEXPHELPERContainer #mainMenu>li ul.opened{height:auto;-webkit-transform:translateX(0);-moz-transform:translateX(0);-ms-transform:translateX(0);-o-transform:translateX(0);transform:translateX(0)}#WYNEXPHELPERContainer ul.info li{font-size:12px;padding:5px 10px 0}#WYNEXPHELPERContainer ul.info li:first-child{padding-top:10px}#WYNEXPHELPERContainer ul.info li:last-child{padding-bottom:10px}`;

    // All of the actual functionality lives here
    const js = function() {
        let menu = document.querySelector('#WYNEXPHELPERContainer');
        let mainMenu = menu.querySelector('#mainMenu');
        let menuOpener = menu.querySelector('.openMenu');
        let menuCloser = menu.querySelector('.closeMenu');

        const closeMenu = (menu) => {
            menu.classList.remove('open');
            menu.classList.add('closed');
        };

        const openMenu = menu => {
            menu.classList.remove('closed');
            menu.classList.add('open');
        };

        const toggleClass = (el, className) => {
            if(el.classList.contains(className)) {
                el.classList.remove(className);
            } else {
                el.classList.add(className);   
            }
        };

        const openUl = target => {
            ulToOpen = target.nextSibling.nextSibling;
            toggleClass(target, 'active');
            toggleClass(ulToOpen, 'opened');
        };

        // Attempts to figure out the name, origin and location of the current theme
		const getThemeDirectoryInfo = () => {
			let themeInfo = {};
            // Want to know if this is theme designer based
			if(document.getElementById('styles-foundation') === null) { 
				console.log('no foundation theme');
				return "No foundation theme detected - Likely a theme designer based theme"; 
			} else {
                // If not it probably comes from themekit or is a foundation theme
				const themeUrl = document.getElementById('styles-foundation').href;
				splitUrl = themeUrl.split('/');
				if(splitUrl[3] === "client") {
					themeInfo.type = "Themekit theme, likely built by services";
					themeInfo.themeDesigner = false;
					themeInfo.clientDirectory = splitUrl[4];
					themeInfo.themeDirectory = splitUrl[5];
					return `${themeInfo.type} | ${themeInfo.clientDirectory}/${themeInfo.themeDirectory}`;
				} else if (splitUrl[3] === "themes") {
					themeInfo.type = "System theme";
					themeInfo.themeDesigner = false;
					themeInfo.themeDirectory = splitUrl[4];
					return `${themeInfo.type} | Theme: ${themeInfo.themeDirectory}`;
				}
			} 
		}

        // Populate the template with the appropriate information
        const addData = () => {
            let datas;
            // If NGX doesnt exist then something may have gone wrong, we want to provide dummy data
            if(typeof NGX != "undefined") {
                // All of the info
                datas = {
                    acctId : NGX.App.config.tenant,
                    acctLink: 'https://app.engagesciences.com/m/account/stats/' + NGX.App.config.tenant,
                    campaignId : NGX.info.id,
                    campaignLink: 'https://app.engagesciences.com/admin/campaign/show/' + NGX.info.id,
                    campaignName : NGX.info.name,
                    themeDirectory : getThemeDirectoryInfo()
                };
            } else {
                datas = {
                    acctId : "1234",
                    acctLink : "#foo",
                    campaignId : "12345",
                    campaignLink : "#bar",
                    campaignName : "Campaign Name",
                    themeDirectory : "Theme-1"
                };
            }

            // Populate the template with the data
            for (var data in datas) {
                var els = menu.querySelectorAll('[data-info="' + data + '"]');
                for(var i = 0; i < els.length; i++) {
                    if (els[i].tagName === "A") {
                        els[i].href = datas[data];
                    } else {
                        els[i].innerHTML = datas[data];
                    }
                }
            }
        };

        mainMenu.addEventListener('click', function(e) {
            e = e || window.event;
            let target = e.target || e.srcElement;

            if(target.nodeName == 'A') {
                openUl(target);
            }
        }, false);

        menuCloser.addEventListener('click', function(e) {
            menu.classList.remove('open');
        }, false);

        menuOpener.addEventListener('click', function() {
            menu.classList.add('open');
        }, false);

		// if NGX exists, run addData();
		addData();

		menu.addEventListener('click', function(e){
			var href = e.target.closest('a').href || '';
			if(href === window.location.href + '#') {
				e.preventDefault();
			}
		}, false);
    };

    const init = function() {
        // Add blank template to page
        const body = document.querySelector('body');
        const theHelper = document.createElement('div');
        theHelper.id = "WYNEXPHELPERContainer";
        theHelper.innerHTML = template;
		body.appendChild(theHelper);
        
        // Add styles to page
        let css = document.createElement('style');
        css.id = "WYNEXPHELPER-styles";
        css.innerHTML = styles;
        body.appendChild(css);

        // Do the functional stuff
        js();
    };

    return {
        init: init
    };
})().init();